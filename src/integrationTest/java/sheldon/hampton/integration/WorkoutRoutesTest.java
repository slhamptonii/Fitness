package sheldon.hampton.integration;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import sheldon.hampton.controller.WorkoutController;
import sheldon.hampton.dao.WorkoutDao;
import sheldon.hampton.exception.GainsLogExceptionHandler;
import sheldon.hampton.filter.SimpleCORSFilter;
import sheldon.hampton.integration.context.DaoContext;
import sheldon.hampton.service.WorkoutService;
import sheldon.hampton.utility.JsonUtil;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Sheldon
 * @date 0021, 21, December, 2017
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WorkoutController.class, WorkoutService.class, WorkoutDao.class,
        JsonUtil.class, SimpleCORSFilter.class, DaoContext.class, GainsLogExceptionHandler.class})
public class WorkoutRoutesTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutRoutesTest.class);
    private MockMvc mockMvc;
    private static DatabaseUtil databaseUtil = new DatabaseUtil();

    @Autowired
    private WorkoutController workoutController;

    @BeforeClass
    public static void prep(){
        String user = "{\"name\":\"Testie\",\"email\":\"my@email.net\",\"password\":\"retard\"}";
        databaseUtil.insertUsers(Collections.singletonList(user));
    }

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(workoutController).setControllerAdvice(new GainsLogExceptionHandler()).build();
        LOGGER.info("controller setup");
    }

    @Test
    public void getAll() throws Exception {
        databaseUtil.deleteWorkout("Full Body - 1");
        databaseUtil.deleteWorkout("Full Body - 2");

        String workout = "{\"name\":\"Full Body - 1\",\"userName\":\"Testie\",\"calendarName\":\"Feb\",\"exercises\":[{\"name\":\"squat\",\"muscleGroup\":\"Legs\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"}]}]}";
        String workout2 = "{\"name\":\"Full Body - 2\",\"userName\":\"Testie\",\"calendarName\":\"Feb\",\"exercises\":[{\"name\":\"sprint\",\"muscleGroup\":\"Heart\",\"superSet\":null,\"sets\":[{\"weight\":\"100\",\"reps\":\"10\"}]}]}";
        List<String> workoutList = new LinkedList<>();
        workoutList.add(workout);
        workoutList.add(workout2);

        databaseUtil.insertWorkouts(workoutList);

        mockMvc.perform(get("/users/Testie/workouts").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*]", hasSize(2)))
        ;
    }

    @Test
    public void getSingle() throws Exception {
        databaseUtil.deleteWorkout("Full Body - 1");

        String workout = "{\"name\":\"Full Body - 1\",\"userName\":\"Testie\",\"calendarName\":\"Feb\",\"exercises\":[{\"name\":\"squat\",\"muscleGroup\":\"Legs\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"}]}]}";

        databaseUtil.insertWorkouts(Collections.singletonList(workout));

        mockMvc.perform(get("/users/Testie/workouts/Full Body - 1").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Full Body - 1")))
            .andExpect(jsonPath("$.calendarName", is("Feb")))
            .andExpect(jsonPath("$.createdDateTime", notNullValue()))
            .andExpect(jsonPath("$.updatedDateTime", notNullValue()))
            .andExpect(jsonPath("$.exercises[*]", hasSize(1)))
            .andExpect(jsonPath("$.exercises[0].name", is("squat")))
            .andExpect(jsonPath("$.exercises[0].muscleGroup", is("Legs")))
            .andExpect(jsonPath("$.exercises[0].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[0].sets[*]", hasSize(1)))
            .andExpect(jsonPath("$.exercises[0].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[0].sets[0].reps", is("10")))
        ;
    }

    @Test
    public void create() throws Exception {
        databaseUtil.deleteWorkout("Full Body - 1");

        String workout = "{\"name\":\"Full Body - 1\",\"userName\":\"Testie\",\"calendarName\":\"Feb\",\"exercises\":[" +
            "{\"name\":\"bench\",\"muscleGroup\":\"Chest\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}," +
            "{\"name\":\"pullup\",\"muscleGroup\":\"Back\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}," +
            "{\"name\":\"shrugs\",\"muscleGroup\":\"Shoulders\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}," +
            "{\"name\":\"legLifts\",\"muscleGroup\":\"Core\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}," +
            "{\"name\":\"curl\",\"muscleGroup\":\"Arms\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}," +
            "{\"name\":\"sprint\",\"muscleGroup\":\"Heart\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}," +
            "{\"name\":\"squat\",\"muscleGroup\":\"Legs\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}]}"
        ;

        mockMvc.perform(post("/users/Testie/workouts").contentType(MediaType.APPLICATION_JSON).content(workout))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Full Body - 1")))
            .andExpect(jsonPath("$.calendarName", is("Feb")))
            .andExpect(jsonPath("$.createdDateTime", notNullValue()))
            .andExpect(jsonPath("$.updatedDateTime", notNullValue()))
            .andExpect(jsonPath("$.exercises[*]", hasSize(7)))
            .andExpect(jsonPath("$.exercises[0].name", is("bench")))
            .andExpect(jsonPath("$.exercises[0].muscleGroup", is("Chest")))
            .andExpect(jsonPath("$.exercises[0].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[0].sets[*]", hasSize(5)))
            .andExpect(jsonPath("$.exercises[0].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[0].sets[0].reps", is("10")))
            .andExpect(jsonPath("$.exercises[1].name", is("pullup")))
            .andExpect(jsonPath("$.exercises[1].muscleGroup", is("Back")))
            .andExpect(jsonPath("$.exercises[1].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[1].sets[*]", hasSize(5)))
            .andExpect(jsonPath("$.exercises[1].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[1].sets[0].reps", is("10")))
            .andExpect(jsonPath("$.exercises[2].name", is("shrugs")))
            .andExpect(jsonPath("$.exercises[2].muscleGroup", is("Shoulders")))
            .andExpect(jsonPath("$.exercises[2].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[2].sets[*]", hasSize(5)))
            .andExpect(jsonPath("$.exercises[2].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[2].sets[0].reps", is("10")))
            .andExpect(jsonPath("$.exercises[3].name", is("legLifts")))
            .andExpect(jsonPath("$.exercises[3].muscleGroup", is("Core")))
            .andExpect(jsonPath("$.exercises[3].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[3].sets[*]", hasSize(5)))
            .andExpect(jsonPath("$.exercises[3].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[3].sets[0].reps", is("10")))
            .andExpect(jsonPath("$.exercises[4].name", is("curl")))
            .andExpect(jsonPath("$.exercises[4].muscleGroup", is("Arms")))
            .andExpect(jsonPath("$.exercises[4].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[4].sets[*]", hasSize(5)))
            .andExpect(jsonPath("$.exercises[4].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[4].sets[0].reps", is("10")))
            .andExpect(jsonPath("$.exercises[5].name", is("sprint")))
            .andExpect(jsonPath("$.exercises[5].muscleGroup", is("Heart")))
            .andExpect(jsonPath("$.exercises[5].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[5].sets[*]", hasSize(5)))
            .andExpect(jsonPath("$.exercises[5].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[5].sets[0].reps", is("10")))
            .andExpect(jsonPath("$.exercises[6].name", is("squat")))
            .andExpect(jsonPath("$.exercises[6].muscleGroup", is("Legs")))
            .andExpect(jsonPath("$.exercises[6].superSet", nullValue()))
            .andExpect(jsonPath("$.exercises[6].sets[*]", hasSize(5)))
            .andExpect(jsonPath("$.exercises[6].sets[0].weight", is("10")))
            .andExpect(jsonPath("$.exercises[6].sets[0].reps", is("10")))
        ;

    }
}
