package sheldon.hampton.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author Sheldon
 * @date 0021, 21, December, 2017
 */
public class DatabaseUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseUtil.class);

    private static DriverManagerDataSource driverManagerDataSource;
    private static Connection connection;
    private static Statement statement;

    DatabaseUtil(){
        if(null == driverManagerDataSource){
            final String driver = "org.postgresql.Driver";
            final String url = "jdbc:postgresql://localhost:5432/Fitness";
            final String username = "postgres";
            final String password = "_nodlehs93";
            driverManagerDataSource = new DriverManagerDataSource(url, username, password);
            driverManagerDataSource.setDriverClassName(driver);

            try{
                if(null == connection){
                    connection = driverManagerDataSource.getConnection();
                    statement = connection.createStatement();
                }
            }catch (SQLException ex){
                LOGGER.error("failed to connect", ex);
            }
        }
    }

    void insertWorkouts(List<String> workouts){
        String insert = "insert into workout (workout) values(to_json('%s'::json))";

        workouts.forEach(workout -> {
            try {
                statement.execute(String.format(insert, workout));
            } catch (SQLException e) {
                LOGGER.error("error inserting workout", e);
            }
        });
    }

    void deleteWorkout(String name){
        String delete = "delete from public.workout where workout->>'name' = '%s' and workout->>'userName' = 'Testie'";

        try{
            statement.execute(String.format(delete, name));
        }catch (SQLException e){
            LOGGER.error("error deleting workout", e);
        }
    }

    void insertUsers(List<String> users){
        String insert = "insert into public.user (user_json) values(to_json('%s'::json))";

        users.forEach(user -> {
            try {
                statement.execute(String.format(insert, user));
            } catch (SQLException e) {
                LOGGER.error("error inserting user", e);
            }
        });
    }

    void deleteUser(){
        String delete = "delete from public.user where user_json->>'name' = 'Testie'";

        try{
            statement.execute(delete);
        }catch (SQLException e){
            LOGGER.error("error deleting user", e);
        }
    }
}
