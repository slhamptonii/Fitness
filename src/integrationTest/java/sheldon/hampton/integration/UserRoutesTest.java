package sheldon.hampton.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import sheldon.hampton.controller.UserController;
import sheldon.hampton.dao.UserDao;
import sheldon.hampton.exception.GainsLogExceptionHandler;
import sheldon.hampton.filter.SimpleCORSFilter;
import sheldon.hampton.integration.context.DaoContext;
import sheldon.hampton.service.UserService;
import sheldon.hampton.utility.JsonUtil;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Sheldon
 * @date 0020, 20, January, 2018
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {UserController.class, UserService.class, UserDao.class,
        JsonUtil.class, SimpleCORSFilter.class, DaoContext.class, GainsLogExceptionHandler.class})
public class UserRoutesTest {
    private MockMvc mockMvc;

    private DatabaseUtil databaseUtil = new DatabaseUtil();

    @Autowired
    private UserController userController;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(userController).setControllerAdvice(new GainsLogExceptionHandler()).build();
    }

    @Test
    public void create() throws Exception{
        databaseUtil.deleteUser();

        String user = "{\"name\":\"Testie\",\"email\":\"my@email.net\",\"password\":\"retard\"}";

        mockMvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON).content(user))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Testie")))
                .andExpect(jsonPath("$.email", is("my@email.net")))
                .andExpect(jsonPath("$.password", is("retard")))
        ;
    }

    @Test
    public void getDetail() throws Exception{
        databaseUtil.deleteUser();

        String user = "{\"name\":\"Testie\",\"email\":\"my@email.net\",\"password\":\"retard\"}";
        databaseUtil.insertUsers(Collections.singletonList(user));

        mockMvc.perform(get("/users/Testie").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Testie")))
            .andExpect(jsonPath("$.email", is("my@email.net")))
            .andExpect(jsonPath("$.password", is("retard")))
        ;

    }

    @Test
    public void update() throws Exception{
        databaseUtil.deleteUser();

        String user = "{\"name\":\"Testie\",\"email\":\"my@email.net\",\"password\":\"retard\"}";
        databaseUtil.insertUsers(Collections.singletonList(user));

        String update = "{\"name\":\"Testie\",\"email\":\"my@email.org\",\"password\":\"puke\"}";

        mockMvc.perform(post("/users/Testie").contentType(MediaType.APPLICATION_JSON).content(update))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Testie")))
                .andExpect(jsonPath("$.email", is("my@email.org")))
                .andExpect(jsonPath("$.password", is("puke")))
        ;

    }

    @Test
    public void deleteUser() throws Exception{
        String user = "{\"name\":\"Testie\",\"email\":\"my@email.net\",\"password\":\"retard\"}";
        databaseUtil.insertUsers(Collections.singletonList(user));

        mockMvc.perform(delete("/users/Testie").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
