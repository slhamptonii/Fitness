package sheldon.hampton.integration.context;

import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * @author Sheldon
 * @date 0021, 21, December, 2017
 */
@Configuration
@ComponentScan("sheldon.hampton.dao, sheldon.hampton.utility")
@PropertySource({"classpath:application-queries.properties"})
public class DaoContext {

    @Bean
    @Scope("singleton")
    public DataSource dataSource(){
        final String driver = "org.postgresql.Driver";
        final String url = "jdbc:postgresql://localhost:5432/Fitness";
        final String username = "postgres";
        final String password = "_nodlehs93";
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(url, username, password);
        driverManagerDataSource.setDriverClassName(driver);
        return driverManagerDataSource;
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(){
        return new NamedParameterJdbcTemplate(dataSource());
    }

    @Bean
    public JdbcTemplate jdbcTemplate(){
        return new JdbcTemplate(dataSource());
    }
}
