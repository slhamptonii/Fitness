package sheldon.hampton.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import sheldon.hampton.controller.AuthController;
import sheldon.hampton.dao.UserDao;
import sheldon.hampton.exception.GainsLogExceptionHandler;
import sheldon.hampton.filter.SimpleCORSFilter;
import sheldon.hampton.integration.context.DaoContext;
import sheldon.hampton.service.UserService;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Sheldon
 * @date 0001, 1, February, 2018
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AuthController.class, UserDao.class,
        UserService.class, SimpleCORSFilter.class, DaoContext.class, GainsLogExceptionHandler.class})
public class AuthRoutesTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthRoutesTest.class);
    private MockMvc mockMvc;
    private DatabaseUtil databaseUtil = new DatabaseUtil();

    @Autowired
    private AuthController authController;

    @Before
    public void setup(){
        String user = "{\"name\":\"Testie\",\"email\":\"my@email.net\",\"password\":\"retard\"}";
        databaseUtil.insertUsers(Collections.singletonList(user));
        mockMvc = MockMvcBuilders.standaloneSetup(authController).setControllerAdvice(new GainsLogExceptionHandler()).build();
        LOGGER.info("controller setup");
    }

    @Test
    public void passAuthentication() throws Exception {
        String credentials = "{\"eman\":\"Testie\",\"drowssap\":\"retard\"}";

        mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(credentials))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Testie")))
            .andExpect(jsonPath("$.email", is("my@email.net")))
            .andExpect(jsonPath("$.password", is("retard")))
        ;

        databaseUtil.deleteUser();
    }

    @Test
    public void failAuthentication() throws Exception {
        String credentials = "{\"eman\":\"Testie\",\"drowssap\":\"genius\"}";

        mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(credentials))
            .andExpect(status().isForbidden())
        ;

        databaseUtil.deleteUser();
    }
}
