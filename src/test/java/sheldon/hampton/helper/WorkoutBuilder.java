package sheldon.hampton.helper;

import sheldon.hampton.entity.Exercise;
import sheldon.hampton.entity.Workout;

import java.util.List;

/**
 * @author Sheldon
 * @date 0028, 28, January, 2018
 */
public class WorkoutBuilder {

    private Workout workout;

    public WorkoutBuilder(){
        workout = new Workout();
    }

    public Workout build(){
        return workout;
    }

    public WorkoutBuilder withName(String name){
        workout.setName(name);
        return this;
    }

    public WorkoutBuilder withUserName(String userName){
        workout.setUserName(userName);
        return this;
    }

    public WorkoutBuilder withCalendarName(String calendarName){
        workout.setCalendarName(calendarName);
        return this;
    }

    public WorkoutBuilder withExercises(List<Exercise> exercises){
        workout.setExercises(exercises);
        return this;
    }
}
