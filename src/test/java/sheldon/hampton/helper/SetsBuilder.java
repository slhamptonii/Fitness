package sheldon.hampton.helper;

import sheldon.hampton.entity.Sets;

/**
 * @author Sheldon
 * @date 0028, 28, January, 2018
 */
public class SetsBuilder {

    private Sets sets;

    public SetsBuilder(){
        sets = new Sets();
    }

    public Sets build(){
        return sets;
    }

    public SetsBuilder withReps(String reps){
        sets.setReps(reps);
        return this;
    }

    public SetsBuilder withWeight(String weight){
        sets.setWeight(weight);
        return this;
    }
}
