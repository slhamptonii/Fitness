package sheldon.hampton.helper;

import sheldon.hampton.entity.Exercise;
import sheldon.hampton.entity.Sets;
import sheldon.hampton.entity.Workout;
import sheldon.hampton.enums.MuscleGroupEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;

/**
 * @author Sheldon
 * @date 0028, 28, January, 2018
 */
public class TestWorkout {

    private SetsBuilder setsBuilder = new SetsBuilder();

    private WorkoutBuilder workoutBuilder = new WorkoutBuilder();

    private List<Sets> sets = new ArrayList<>();
    private List<Exercise> exercises = new ArrayList<>();

    public Workout dummyWorkout(){
        buildMatchingSets();
        exercises.addAll(asList(bench(), pullup(), shrugs(), legLifts(), curl(), sprint(), squat()));
        workoutBuilder.withExercises(exercises);
        workoutBuilder.withName("Full Body - 1");
        workoutBuilder.withCalendarName("Feb");
        workoutBuilder.withUserName("Testie");
        return workoutBuilder.build();
    }

    private void buildMatchingSets(){
        IntStream.range(0, 5).forEach(index -> {
            setsBuilder.withReps("10");
            setsBuilder.withWeight("10");
            sets.add(setsBuilder.build());
        });
    }

    /*
    build one exercise per enum
    all have 10/10 reps/weight
     */

    public Exercise bench(){
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        exerciseBuilder.withMuscleGroup(MuscleGroupEnum.Chest);
        exerciseBuilder.withSets(sets);
        exerciseBuilder.withName("bench");
        return exerciseBuilder.build();
    }

    public Exercise pullup(){
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        exerciseBuilder.withMuscleGroup(MuscleGroupEnum.Back);
        exerciseBuilder.withSets(sets);
        exerciseBuilder.withName("pullup");
        return exerciseBuilder.build();
    }

    public Exercise shrugs(){
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        exerciseBuilder.withMuscleGroup(MuscleGroupEnum.Shoulders);
        exerciseBuilder.withSets(sets);
        exerciseBuilder.withName("shrugs");
        return exerciseBuilder.build();
    }

    public Exercise curl(){
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        exerciseBuilder.withMuscleGroup(MuscleGroupEnum.Arms);
        exerciseBuilder.withSets(sets);
        exerciseBuilder.withName("curl");
        return exerciseBuilder.build();
    }

    public Exercise legLifts(){
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        exerciseBuilder.withMuscleGroup(MuscleGroupEnum.Core);
        exerciseBuilder.withSets(sets);
        exerciseBuilder.withName("legLifts");
        return exerciseBuilder.build();
    }

    public Exercise sprint(){
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        exerciseBuilder.withMuscleGroup(MuscleGroupEnum.Heart);
        exerciseBuilder.withSets(sets);
        exerciseBuilder.withName("sprint");
        return exerciseBuilder.build();
    }

    public Exercise squat(){
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        exerciseBuilder.withMuscleGroup(MuscleGroupEnum.Legs);
        exerciseBuilder.withSets(sets);
        exerciseBuilder.withName("squat");
        return exerciseBuilder.build();
    }
}
