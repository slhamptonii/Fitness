package sheldon.hampton.helper;

import sheldon.hampton.entity.Exercise;
import sheldon.hampton.entity.Sets;
import sheldon.hampton.enums.MuscleGroupEnum;

import java.util.List;

/**
 * @author Sheldon
 * @date 0028, 28, January, 2018
 */
public class ExerciseBuilder {

    private Exercise exercise;

    public ExerciseBuilder(){
        exercise = new Exercise();
    }

    public Exercise build(){
        return exercise;
    }

    public ExerciseBuilder withName(String name){
        exercise.setName(name);
        return this;
    }

    public ExerciseBuilder withMuscleGroup(MuscleGroupEnum muscleGroup){
        exercise.setMuscleGroup(muscleGroup.name());
        return this;
    }

    public ExerciseBuilder withSets(List<Sets> sets){
        exercise.setSets(sets);
        return this;
    }

    public ExerciseBuilder wiithSuperSet(List<String> superSet){
        exercise.setSuperSet(superSet);
        return this;
    }
}
