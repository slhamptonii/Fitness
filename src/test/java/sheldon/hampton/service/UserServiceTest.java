package sheldon.hampton.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import sheldon.hampton.dao.UserDao;
import sheldon.hampton.entity.Role;
import sheldon.hampton.entity.User;
import sheldon.hampton.enums.RoleTypeEnum;
import sheldon.hampton.exception.GainsLogAuthenticationException;
import sheldon.hampton.exception.GainsLogException;

import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Sheldon
 * @date 0011, 11, March, 2018
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserService userService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void createUser() throws IOException {
        User user = new User("quin", "ao", "chop@stick.rice",
                singletonList(new Role(RoleTypeEnum.Vagabond.name())));

        when(userDao.createUser(anyString())).thenReturn(1);

        int result = userService.createUser(user);

        assertThat(result, is(1));
    }

    @Test
    public void createUserGainsLogException(){
        expectedException.expectMessage("TEST ERROR");
        expectedException.expect(GainsLogException.class);

        when(userDao.createUser(anyString())).thenThrow(new GainsLogException(new Exception("TEST ERROR")));

        userService.createUser(new User());
    }


    @Test
    public void selectUser(){
        User user = new User("quin", "ao", "chop@stick.rice",
                singletonList(new Role(RoleTypeEnum.Vagabond.name())));

        when(userDao.selectUser(anyString())).thenReturn(user);

        User result = userService.selectUser("blah");

        assertThat(result, is(user));
    }

    @Test
    public void selectUserGainsLogException(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("TEST");

        when(userDao.selectUser(anyString())).thenThrow(new GainsLogException(new Exception("TEST")));

        userService.selectUser("sadf");
    }

    @Test
    public void selectAllUsers(){
        List<User> userList = asList(new User(), new User());
        when(userDao.selectAllUsers()).thenReturn(userList);

        List<User> result = userService.selectAllUsers();

        assertThat(result, is(userList));
    }

    @Test
    public void selectAllUsersGainsLogException(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("TEST");

        when(userDao.selectAllUsers()).thenThrow(new GainsLogException(new Exception("TEST")));

        userService.selectAllUsers();
    }

    @Test
    public void isAuthenticated(){
        String name = "winston";
        String password = "churchill";
        User user = new User();
        user.setPassword(password);
        user.setName(name);

        when(userDao.selectUser(name)).thenReturn(user);

        User result = userService.isAuthenticated(name, password);

        assertThat(result.getName(), is(user.getName()));
        assertThat(result.getPassword(), is(user.getPassword()));
    }

    @Test
    public void isAuthenticatedNullUser(){
        expectedException.expect(GainsLogAuthenticationException.class);
        expectedException.expectMessage("User: winston is forbidden");

        when(userDao.selectUser("winston")).thenReturn(null);

        userService.isAuthenticated("winston", "");
    }

    @Test
    public void isAuthenticatedWrongUser(){
        String name = "winston";
        String password = "churchill";
        User user = new User();
        user.setPassword(password);
        user.setName(name);

        expectedException.expect(GainsLogAuthenticationException.class);
        expectedException.expectMessage("User: winston is forbidden");

        when(userDao.selectUser(name)).thenReturn(user);

        userService.isAuthenticated("winston", "");
    }
}
