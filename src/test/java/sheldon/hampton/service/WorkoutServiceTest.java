package sheldon.hampton.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import sheldon.hampton.dao.WorkoutDao;
import sheldon.hampton.entity.Workout;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogAuthorizationException;
import sheldon.hampton.exception.GainsLogException;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * @author Sheldon
 * @date 0016, 16, June, 2017
 */
@RunWith(MockitoJUnitRunner.class)
public class WorkoutServiceTest {
    @Mock
    private WorkoutDao dao;

    @InjectMocks
    private WorkoutService service;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void selectAll(){
        List<Workout> workouts = asList(new Workout(), new Workout());
        when(dao.selectAllWorkouts("balls")).thenReturn(workouts);

        List<Workout> result = service.selectAllWorkouts("balls");

        assertThat(result.size(), is(2));
    }

    @Test
    public void selectAllGainsLogException(){
        expectedException.expect(GainsLogAuthorizationException.class);
        expectedException.expectMessage("some exception");

        when(dao.selectAllWorkouts("apple")).thenThrow(new GainsLogAuthorizationException("some exception", ExceptionCode.AUTHORIZATION, new DataIntegrityViolationException("ahhh")));

        service.selectAllWorkouts("apple");
    }

    @Test
    public void selectAllException(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("Error selecting workouts user: apple");

        when(dao.selectAllWorkouts("apple")).thenThrow(new DataIntegrityViolationException("ahhh"));

        service.selectAllWorkouts("apple");
    }

    @Test
    public void select(){
        Workout testWorkout = new Workout();
        when(dao.selectWorkout("blah", "blah")).thenReturn(testWorkout);

        Workout result = service.selectWorkout("blah", "blah");
        assertThat(result, is(testWorkout));
    }

    @Test
    public void selectGainsLogException(){
        expectedException.expect(GainsLogAuthorizationException.class);
        expectedException.expectMessage("some exception");

        when(dao.selectWorkout("blah", "blah")).thenThrow(new GainsLogAuthorizationException("some exception", ExceptionCode.AUTHORIZATION, new DataIntegrityViolationException("ahhh")));

        service.selectWorkout("blah", "blah");
    }

    @Test
    public void selectException(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("Error selecting workout: blah for user: blah");

        when(dao.selectWorkout("blah", "blah")).thenThrow(new DataIntegrityViolationException("ahhh"));

        service.selectWorkout("blah", "blah");
    }

    @Test
    public void delete(){
        when(dao.deleteWorkout("blah", "blah")).thenReturn(1);

        assertThat(service.deleteWorkout("blah", "blah"), is(1));
    }

    @Test
    public void deleteGainsLogException(){
        expectedException.expect(GainsLogAuthorizationException.class);
        expectedException.expectMessage("some exception");

        when(dao.deleteWorkout("blah", "blah")).thenThrow(new GainsLogAuthorizationException("some exception", ExceptionCode.AUTHORIZATION, new DataIntegrityViolationException("ahhh")));

        service.deleteWorkout("blah", "blah");
    }

    @Test
    public void deleteException(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("Error deleting workout: blah for user: blah");

        when(dao.deleteWorkout("blah", "blah")).thenThrow(new DataIntegrityViolationException("ahhh"));

        service.deleteWorkout("blah", "blah");
    }

    @Test
    public void create() {
        when(dao.createWorkout(anyString())).thenReturn(1);

        assertThat(service.createWorkout(new Workout()), is(1));
    }

    @Test
    public void createGainsLogException(){
        expectedException.expect(GainsLogAuthorizationException.class);
        expectedException.expectMessage("some exception");

        when(dao.createWorkout(anyString())).thenThrow(new GainsLogAuthorizationException("some exception", ExceptionCode.AUTHORIZATION, new DataIntegrityViolationException("ahhh")));

        service.createWorkout(new Workout());
    }

    @Test
    public void createException(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("Error creating workout");

        when(dao.createWorkout(anyString())).thenThrow(new DataIntegrityViolationException("ahhh"));

        service.createWorkout(new Workout());
    }

    @Test
    public void update() {
        Workout testWorkout = new Workout();
        testWorkout.setName("blah, blah");
        testWorkout.setUserName("bho");
        when(dao.updateWorkout(anyString(), eq("bho"), eq("blah, blah"))).thenReturn(1);

        assertThat(service.updateWorkout(testWorkout), is(1));
    }

    @Test
    public void updateGainsLogException(){
        expectedException.expect(GainsLogAuthorizationException.class);
        expectedException.expectMessage("some exception");

        Workout testWorkout = new Workout();
        testWorkout.setName("blah, blah");
        testWorkout.setUserName("bho");
        when(dao.updateWorkout(anyString(), eq("bho"), eq("blah, blah"))).thenThrow(new GainsLogAuthorizationException("some exception", ExceptionCode.AUTHORIZATION, new DataIntegrityViolationException("ahhh")));

        service.updateWorkout(testWorkout);
    }

    @Test
    public void updateException(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("Error updating workout");

        Workout testWorkout = new Workout();
        testWorkout.setName("blah, blah");
        testWorkout.setUserName("bho");
        when(dao.updateWorkout(anyString(), eq("bho"), eq("blah, blah"))).thenThrow(new DataIntegrityViolationException("ahhh"));

        service.updateWorkout(testWorkout);
    }
}
