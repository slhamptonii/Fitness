package sheldon.hampton.exception;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import sheldon.hampton.entity.ErrorMessage;
import sheldon.hampton.enums.ExceptionCode;

import java.util.zip.DataFormatException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 * @author Sheldon
 * @date 0011, 11, February, 2018
 */
@RunWith(MockitoJUnitRunner.class)
public class GainsLogExceptionHandlerTest {
    private GainsLogExceptionHandler handler = new GainsLogExceptionHandler();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void handleGainsLogAuthenticationException(){
        GainsLogAuthenticationException authenticationException = new GainsLogAuthenticationException("howdy", ExceptionCode.AUTHORIZATION, new DataFormatException());
        ErrorMessage message = handler.handleGainsLogException(authenticationException);
        assertThat(message.getErrors(), hasSize(1));
        assertThat(message.getErrors().get(0), is("howdy"));
    }

    @Test
    public void handleGainsLogAuthorizationException(){
        GainsLogAuthorizationException authorizationException = new GainsLogAuthorizationException("sup", ExceptionCode.AUTHORIZATION, new DataFormatException());
        ErrorMessage message = handler.handleGainsLogException(authorizationException);
        assertThat(message.getErrors(), hasSize(1));
        assertThat(message.getErrors().get(0), is("sup"));
    }

    @Test
    public void handleGainsLogBadRequestException(){
        GainsLogBadRequestException badRequestException = new GainsLogBadRequestException("aye", ExceptionCode.AUTHORIZATION, new DataFormatException());
        ErrorMessage message = handler.handleGainsLogException(badRequestException);
        assertThat(message.getErrors(), hasSize(1));
        assertThat(message.getErrors().get(0), is("aye"));
    }

    @Test
    public void handleGainsLogException(){
        GainsLogException gainsLogException = new GainsLogException("whaddup", ExceptionCode.AUTHORIZATION, new DataFormatException());
        ErrorMessage message = handler.handleGenericException(gainsLogException);
        assertThat(message.getErrors(), hasSize(1));
        assertThat(message.getErrors().get(0), is("whaddup"));
    }

    @Test
    public void handleGenericException(){
        Exception exception = new Exception("*nods head*");
        ErrorMessage message = handler.handleGenericException(exception);
        assertThat(message.getErrors(), hasSize(1));
        assertThat(message.getErrors().get(0), is("*nods head*"));
    }

    @Test
    public void handleExceptionGeneric(){
        expectedException.expect(GainsLogException.class);
        expectedException.expectMessage("uno");

        GainsLogException exception = new GainsLogException("uno", ExceptionCode.GENERIC, new Exception("dos"));

        handler.handleException(exception);
    }

    @Test
    public void handleExceptionBadRequest(){
        expectedException.expect(GainsLogBadRequestException.class);
        expectedException.expectMessage("un");

        GainsLogException exception = new GainsLogException("un", ExceptionCode.BAD_REQUEST, new Exception("deux"));

        handler.handleException(exception);
    }

    @Test
    public void handleExceptionAuthorization(){
        expectedException.expect(GainsLogAuthorizationException.class);
        expectedException.expectMessage("uno");

        GainsLogException exception = new GainsLogException("uno", ExceptionCode.AUTHORIZATION, new Exception("due"));

        handler.handleException(exception);
    }

    @Test
    public void handleExceptionAuthentication(){
        expectedException.expect(GainsLogAuthenticationException.class);
        expectedException.expectMessage("1");

        GainsLogException exception = new GainsLogException("1", ExceptionCode.AUTHENTICATION, new Exception("dois"));

        handler.handleException(exception);
    }
}
