package sheldon.hampton.utility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import sheldon.hampton.entity.Workout;
import sheldon.hampton.helper.TestWorkout;

import java.io.IOException;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;


/**
 * @author Sheldon
 * @date 0016, 16, June, 2017
 */
@RunWith(MockitoJUnitRunner.class)
public class JsonUtilTest {
    private TestWorkout testWorkout = new TestWorkout();

    @Test
    public void mapObjToStr() throws IOException {
        String result = JsonUtil.mapObjToStr(testWorkout.dummyWorkout());
        assertThat(result, hasJsonPath("$.name", equalTo("Full Body - 1")));
        assertThat(result, hasJsonPath("$.userName", equalTo("Testie")));
        assertThat(result, hasJsonPath("$.calendarName", equalTo("Feb")));
        assertThat(result, hasJsonPath("$.exercises[*]", hasSize(7)));
    }

    @Test
    public void mapStrToObj() throws IOException {
        String workoutJson = "{\"createdDateTime\":1517192405856,\"updatedDateTime\":1517192406008,\"name\":\"Full Body - 1\",\"userName\":\"Testie\",\"calendarName\":\"Feb\",\"exercises\":[{\"name\":\"bench\",\"muscleGroup\":\"Chest\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]},{\"name\":\"pullup\",\"muscleGroup\":\"Back\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]},{\"name\":\"shrugs\",\"muscleGroup\":\"Shoulders\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]},{\"name\":\"legLifts\",\"muscleGroup\":\"Core\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]},{\"name\":\"curl\",\"muscleGroup\":\"Arms\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]},{\"name\":\"sprint\",\"muscleGroup\":\"Heart\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]},{\"name\":\"squat\",\"muscleGroup\":\"Legs\",\"superSet\":null,\"sets\":[{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"},{\"weight\":\"10\",\"reps\":\"10\"}]}]}";
        Workout result = (Workout) JsonUtil.mapStrToObj(workoutJson, Workout.class);

        assertThat(result.getName(), is("Full Body - 1"));
        assertThat(result.getUserName(), is("Testie"));
        assertThat(result.getCalendarName(), is("Feb"));
        assertThat(result.getExercises(), hasSize(7));
        result.getExercises().forEach(exercise -> assertThat(exercise.getSets(), hasSize(5)));
    }
}
