package sheldon.hampton.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import sheldon.hampton.entity.ErrorMessage;

/**
 * @author Sheldon
 * @date 0010, 10, February, 2018
 */
@ControllerAdvice
public class GainsLogExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GainsLogExceptionHandler.class);

    @ExceptionHandler(GainsLogAuthenticationException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public @ResponseBody ErrorMessage handleGainsLogException(GainsLogAuthenticationException ex){
        LOGGER.info(String.format("Handling exception - message: %s", ex.getMessage()));
        return new ErrorMessage(ex.getMessage());
    }

    @ExceptionHandler(GainsLogAuthorizationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody ErrorMessage handleGainsLogException(GainsLogAuthorizationException ex){
        LOGGER.info(String.format("Handling exception - message: %s", ex.getMessage()));
        return new ErrorMessage(ex.getMessage());
    }

    @ExceptionHandler(GainsLogBadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody ErrorMessage handleGainsLogException(GainsLogBadRequestException ex){
        LOGGER.info(String.format("Handling exception - message: %s", ex.getMessage()));
        return new ErrorMessage(ex.getMessage());
    }

    @ExceptionHandler({GainsLogException.class, Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ErrorMessage handleGenericException(Exception ex){
        LOGGER.info(String.format("Handling exception - message: %s", ex.getMessage()));
        return new ErrorMessage(ex.getMessage());
    }

    public static void handleException(GainsLogException exception){
        LOGGER.error(exception.getMessage());
        switch (exception.getExceptionCode()){
            case GENERIC:
                throw exception;
            case BAD_REQUEST:
                throw new GainsLogBadRequestException(exception);
            case AUTHORIZATION:
                throw new GainsLogAuthorizationException(exception);
            case AUTHENTICATION:
                throw new GainsLogAuthenticationException(exception);
        }
    }
}
