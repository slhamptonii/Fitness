package sheldon.hampton.exception;

import sheldon.hampton.enums.ExceptionCode;

/**
 * @author Sheldon
 * @date 0002, 2, February, 2018
 */
public class GainsLogException extends RuntimeException {
    private ExceptionCode exceptionCode;

    public GainsLogException(String cause, ExceptionCode exceptionCode, Throwable throwable){
        super(cause, throwable);
        this.exceptionCode = exceptionCode;
    }

    public GainsLogException(Exception exception){
        super(exception.getMessage(), exception.getCause());
        this.exceptionCode = ExceptionCode.GENERIC;
    }

    public ExceptionCode getExceptionCode() {
        return exceptionCode;
    }
}
