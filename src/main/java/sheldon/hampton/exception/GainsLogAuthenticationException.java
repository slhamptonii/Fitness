package sheldon.hampton.exception;

import sheldon.hampton.enums.ExceptionCode;

/**
 * @author Sheldon
 * @date 0011, 11, February, 2018
 */
public class GainsLogAuthenticationException extends GainsLogException {
    public GainsLogAuthenticationException(String cause, ExceptionCode exceptionCode, Throwable throwable) {
        super(cause, exceptionCode, throwable);
    }

    public GainsLogAuthenticationException(GainsLogException exception){
        super(exception.getMessage(), exception.getExceptionCode(), exception.getCause());
    }
}
