package sheldon.hampton.exception;

import sheldon.hampton.enums.ExceptionCode;

/**
 * @author Sheldon
 * @date 0011, 11, February, 2018
 */
public class GainsLogAuthorizationException extends GainsLogException {
    public GainsLogAuthorizationException(String cause, ExceptionCode exceptionCode, Throwable throwable) {
        super(cause, exceptionCode, throwable);
    }

    public GainsLogAuthorizationException(GainsLogException exception){
        super(exception.getMessage(), exception.getExceptionCode(), exception.getCause());
    }
}
