package sheldon.hampton.exception;

import sheldon.hampton.enums.ExceptionCode;

/**
 * @author Sheldon
 * @date 0011, 11, February, 2018
 */
public class GainsLogBadRequestException extends GainsLogException {
    public GainsLogBadRequestException(String cause, ExceptionCode exceptionCode, Throwable throwable) {
        super(cause, exceptionCode, throwable);
    }

    public GainsLogBadRequestException(GainsLogException exception){
        super(exception.getMessage(), exception.getExceptionCode(), exception.getCause());
    }
}
