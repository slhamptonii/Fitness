package sheldon.hampton.entity;

import org.springframework.boot.jackson.JsonComponent;

/**
 * @author Sheldon
 * @date 0014, 14, January, 2018
 */
@JsonComponent
public class Role {
    private String name;

    public Role() {}

    public Role(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
