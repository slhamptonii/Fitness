package sheldon.hampton.entity;

import org.springframework.boot.jackson.JsonComponent;

/**
 * @author Sheldon
 * @date 0001, 1, February, 2018
 */
@JsonComponent
public class Credentials extends BaseEntity {
    private String eman, drowssap;

    public Credentials() {}

    public Credentials(String eman, String drowssap) {
        this.eman = eman;
        this.drowssap = drowssap;
    }

    public String getEman() {
        return eman;
    }

    public void setEman(String eman) {
        this.eman = eman;
    }

    public String getDrowssap() {
        return drowssap;
    }

    public void setDrowssap(String drowssap) {
        this.drowssap = drowssap;
    }
}
