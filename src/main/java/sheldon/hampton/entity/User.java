package sheldon.hampton.entity;

import org.joda.time.DateTime;
import org.springframework.boot.jackson.JsonComponent;

import java.util.List;

/**
 * @author Sheldon
 * @date 0014, 14, January, 2018
 */
@JsonComponent
public class User extends BaseEntity{
    private String name, password, email;
    private List<Role> roles;

    public User() {}

    public User(String name, String password, String email, List<Role> roles) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(DateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public DateTime getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(DateTime updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
