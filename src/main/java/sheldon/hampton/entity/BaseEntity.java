package sheldon.hampton.entity;

import org.joda.time.DateTime;
import org.springframework.boot.jackson.JsonComponent;

/**
 * @author Sheldon
 * @date 0015, 15, January, 2018
 */
@JsonComponent
public class BaseEntity {
    protected DateTime createdDateTime, updatedDateTime;

    public BaseEntity() {
        this.createdDateTime = new DateTime();
        this.updatedDateTime = new DateTime();
    }

    public DateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(DateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public DateTime getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(DateTime updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }
}
