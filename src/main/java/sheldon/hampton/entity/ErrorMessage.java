package sheldon.hampton.entity;

import org.springframework.boot.jackson.JsonComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Sheldon
 * @date 0011, 11, February, 2018
 */
@JsonComponent
public class ErrorMessage {
    private List<String> errors = new ArrayList<>(0);

    public ErrorMessage(){}

    public ErrorMessage(String error){
        errors.add(error);
    }

    public ErrorMessage(String... errors){
        this.errors.addAll(Arrays.asList(errors));
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
