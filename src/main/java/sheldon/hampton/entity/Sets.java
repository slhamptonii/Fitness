package sheldon.hampton.entity;

import org.springframework.boot.jackson.JsonComponent;

/**
 * @author Sheldon
 * @date 0020, 20, February, 2017
 */
@JsonComponent
public class Sets {

    private String weight, reps;

    public Sets(){}

    public Sets(String weight, String reps) {
        this.weight = weight;
        this.reps = reps;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getReps() {
        return reps;
    }

    public void setReps(String reps) {
        this.reps = reps;
    }
}
