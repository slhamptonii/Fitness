package sheldon.hampton.entity;

import org.springframework.boot.jackson.JsonComponent;

import java.util.List;

/**
 * @author Sheldon
 * @date 0012, 12, March, 2017
 */
@JsonComponent
public class Workout extends BaseEntity{

    private String name, userName, calendarName;

    private List<Exercise> exercises;

    public Workout(){}

    public Workout(String name, String userName, String calendarName, List<Exercise> exercises) {
        this.name = name;
        this.userName = userName;
        this.calendarName = calendarName;
        this.exercises = exercises;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }
}
