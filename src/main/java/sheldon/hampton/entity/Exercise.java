package sheldon.hampton.entity;

import org.springframework.boot.jackson.JsonComponent;

import java.util.List;

@JsonComponent
public class Exercise {

    private String name, muscleGroup;

    private List<String> superSet;

    private List<Sets> sets;

    public Exercise() {}

    public Exercise(String name, String muscleGroup, List<String> superSet, List<Sets> sets) {
        this.sets = sets;
        this.name = name;
        this.muscleGroup = muscleGroup;
        this.superSet = superSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(String muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    public List<String> getSuperSet() {
        return superSet;
    }

    public void setSuperSet(List<String> superSet) {
        this.superSet = superSet;
    }

    public List<Sets> getSets() {
        return sets;
    }

    public void setSets(List<Sets> sets) {
        this.sets = sets;
    }
}