package sheldon.hampton.service;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sheldon.hampton.dao.UserDao;
import sheldon.hampton.entity.User;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogAuthenticationException;
import sheldon.hampton.exception.GainsLogException;
import sheldon.hampton.utility.JsonUtil;

import java.util.ArrayList;
import java.util.List;

import static sheldon.hampton.exception.GainsLogExceptionHandler.handleException;

/**
 * @author Sheldon
 * @date 0014, 14, January, 2018
 */
@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    
    @Autowired
    private UserDao userDao;

    public List<User> selectAllUsers(){
        List<User> users = new ArrayList<>(0);

        try {
            users = userDao.selectAllUsers();
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error selecting users", ExceptionCode.GENERIC, e));
        }

        return users;
    }

    public User selectUser(String name){
        User user = null;

        try {
            user = userDao.selectUser(name);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting user: %s", name), ExceptionCode.GENERIC, e));
        }

        return user;
    }

    public int deleteUserById(String name){
        int result = -1;

        try {
            result = userDao.deleteUser(name);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error deleting user: %s", name), ExceptionCode.GENERIC, e));
        }

        return result;
    }

    public int createUser(User user) {
        int result = -1;

        try {
            user.setCreatedDateTime(new DateTime());
            user.setUpdatedDateTime(new DateTime());
            result = userDao.createUser(JsonUtil.mapObjToStr(user));
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error creating user", ExceptionCode.GENERIC, e));
        }

        return result;
    }

    public int updateUser(User user) {
        int result = -1;

        try {
            user.setUpdatedDateTime(new DateTime());
            result = userDao.updateUser(JsonUtil.mapObjToStr(user), user.getName());
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error updating user", ExceptionCode.GENERIC, e));
        }

        return result;
    }

    public User isAuthenticated(String name, String password){
        User user = null;

        try {
        LOGGER.info(String.format("authenticating %s %s", name, password));
        User suggestedUser = userDao.selectUser(name);

            user = null == suggestedUser ? null : suggestedUser.getPassword().equals(password) ? suggestedUser : null;

            if(user == null){
                throw new GainsLogAuthenticationException(String.format("User: %s is forbidden", name), ExceptionCode.AUTHENTICATION, null);
            }
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error authenticating user: %s", name), ExceptionCode.GENERIC, e));
        }

        return user;
    }
}
