package sheldon.hampton.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sheldon.hampton.dao.WorkoutDao;
import sheldon.hampton.entity.Workout;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogException;
import sheldon.hampton.utility.JsonUtil;

import java.util.ArrayList;
import java.util.List;

import static sheldon.hampton.exception.GainsLogExceptionHandler.handleException;

/**
 * @author Sheldon
 * @date 0013, 13, March, 2017
 */
@Service
public class WorkoutService {

    @Autowired
    private WorkoutDao workoutDao;

    public List<Workout> selectAllWorkouts(String userName){
        List<Workout> workouts = new ArrayList<>(0);

        try {
            workouts = workoutDao.selectAllWorkouts(userName);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting workouts user: %s", userName), ExceptionCode.GENERIC, e));
        }

        return workouts;
    }

    public Workout selectWorkout(String userName, String name){
        Workout workout = null;

        try {
            workout = workoutDao.selectWorkout(userName, name);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting workout: %s for user: %s", name, userName), ExceptionCode.GENERIC, e));
        }

        return workout;
    }

    public int deleteWorkout(String userName, String name){
        int result = -1;

        try {
            result = workoutDao.deleteWorkout(userName, name);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error deleting workout: %s for user: %s", name, userName), ExceptionCode.GENERIC, e));
        }

        return result;
    }

    public int createWorkout(Workout workout) {
        int result = -1;

        try {
            workout.setCreatedDateTime(new DateTime());
            workout.setUpdatedDateTime(new DateTime());
            result = workoutDao.createWorkout(JsonUtil.mapObjToStr(workout));
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error creating workout", ExceptionCode.GENERIC, e));
        }

        return result;
    }

    public int updateWorkout(Workout workout) {
        int result = -1;

        try {
            workout.setUpdatedDateTime(new DateTime());
            result = workoutDao.updateWorkout(JsonUtil.mapObjToStr(workout), workout.getUserName(), workout.getName());
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error updating workout", ExceptionCode.GENERIC, e));
        }

        return result;
    }
}
