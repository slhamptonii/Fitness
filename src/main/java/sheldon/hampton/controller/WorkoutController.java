package sheldon.hampton.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import sheldon.hampton.entity.Workout;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogException;
import sheldon.hampton.service.WorkoutService;

import java.util.ArrayList;
import java.util.List;

import static sheldon.hampton.exception.GainsLogExceptionHandler.handleException;

/**
 * @author Sheldon
 * @date 0013, 13, March, 2017
 */
@RestController
@RequestMapping("/users/{userName}/workouts")
public class WorkoutController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutController.class);

    @Autowired
    private WorkoutService workoutService;

    @GetMapping
    public @ResponseBody List<Workout> selectAllWorkouts(@PathVariable String userName){
        LOGGER.info(String.format("Retrieving all workouts - userName: %s", userName));
        List<Workout> workouts = new ArrayList<>(0);

        try {
            workouts = workoutService.selectAllWorkouts(userName);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting workouts user: %s", userName), ExceptionCode.GENERIC, e));
        }

        return workouts;
    }

    @GetMapping("/{name}")
    public @ResponseBody Workout selectWorkout(@PathVariable String userName, @PathVariable String name){
        LOGGER.info(String.format("Retrieving workout - userName: %s | name:%s", userName, name));
        Workout workout = null;

        try {
            workout = workoutService.selectWorkout(userName, name);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting workout - userName: %s | name:%s", userName, name), ExceptionCode.GENERIC, e));
        }
        
        return workout;
    }

    @DeleteMapping("/{name}")
    public void deleteWorkout(@PathVariable String userName, @PathVariable String name){
        LOGGER.info(String.format("Deleting workout - userName: %s | name:%s", userName, name));

        try {
            if(workoutService.deleteWorkout(userName, name) != 1){
                throw new GainsLogException("Failed to Delete Workout", ExceptionCode.GENERIC, null);
            }
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error deleting workout - userName: %s | name:%s", userName, name), ExceptionCode.GENERIC, e));
        }
    }

    @PostMapping
    public @ResponseBody Workout createWorkout(@PathVariable String userName, @RequestBody Workout workout) {
        LOGGER.info(String.format("Creating workout - userName: %s", userName));
        if(!userName.equals(workout.getUserName())){
            throw new GainsLogException("Permission Denied - Users do no match", ExceptionCode.AUTHENTICATION, null);
        }

        try {
            if(workoutService.createWorkout(workout) != 1){
                throw new GainsLogException("Failed to create workout", ExceptionCode.GENERIC, null);
            }
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error creating workout - userName: %s", userName), ExceptionCode.GENERIC, e));
        }

        return workout;
    }

    @PostMapping("/{name}")
    public @ResponseBody Workout updateWorkout(@PathVariable String userName, @PathVariable String name, @RequestBody Workout workout) {
        LOGGER.info(String.format("Updating workout - userName: %s | name:%s", userName, name));

        if(!userName.equals(workout.getUserName())){
            throw new GainsLogException("Permission Denied - users do no match", ExceptionCode.AUTHENTICATION, null);
        }

        if(!name.equals(workout.getName())){
            throw new GainsLogException("Bad Request - workout names do no match", ExceptionCode.BAD_REQUEST, null);
        }

        try{
            if(workoutService.updateWorkout(workout) != 1){
                throw new GainsLogException("Failed to update workout", ExceptionCode.GENERIC, null);
            }
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error updating workout - userName: %s", userName), ExceptionCode.GENERIC, e));
        }

        return workout;
    }
}
