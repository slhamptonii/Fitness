package sheldon.hampton.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import sheldon.hampton.entity.Credentials;
import sheldon.hampton.entity.User;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogException;
import sheldon.hampton.service.UserService;

import static sheldon.hampton.exception.GainsLogExceptionHandler.handleException;

/**
 * @author Sheldon
 * @date 0028, 28, January, 2018
 */
@RestController("/login")
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping
    public @ResponseBody User login(@RequestBody Credentials credentials){
        User user = null;

        try {
            user = userService.isAuthenticated(credentials.getEman(), credentials.getDrowssap());
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error authentication user", ExceptionCode.AUTHENTICATION, e));
        }

        return user;
    }
}
