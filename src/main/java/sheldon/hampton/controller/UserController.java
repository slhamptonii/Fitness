package sheldon.hampton.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import sheldon.hampton.entity.User;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogException;
import sheldon.hampton.service.UserService;

import java.util.ArrayList;
import java.util.List;

import static sheldon.hampton.exception.GainsLogExceptionHandler.handleException;

/**
 * @author Sheldon
 * @date 0014, 14, January, 2018
 */
@RestController
@RequestMapping("/users")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping
    public @ResponseBody
    List<User> selectAllUsers(){
        List<User> users = new ArrayList<>(0);
        LOGGER.info("Retrieving all users.");

        try {
            users = userService.selectAllUsers();
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error selecting users", ExceptionCode.GENERIC, e));
        }

        return users;
    }

    @GetMapping("/{name}")
    public @ResponseBody User selectUser(@PathVariable String name){
        User user = null;
        LOGGER.info(String.format("Retrieving user - name:%s", name));

        try {
            user = userService.selectUser(name);
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error selecting user", ExceptionCode.GENERIC, e));
        }

        return  user;
    }

    @DeleteMapping("/{name}")
    public void deleteUser(@PathVariable String name){
        LOGGER.info(String.format("Deleting user - name:%s", name));

        try {
            if (userService.deleteUserById(name) != 1) {
                throw new GainsLogException("Error deleting user", ExceptionCode.GENERIC, null);
            }
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error deleting user", ExceptionCode.GENERIC, e));
        }
    }

    @PostMapping
    public @ResponseBody User createUser(@RequestBody User user) {
        LOGGER.info("Creating user");

        try {
            if (userService.createUser(user) != 1) {
                throw new GainsLogException("Error creating user", ExceptionCode.GENERIC, null);
            }
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error creating user", ExceptionCode.GENERIC, e));
        }

        return user;
    }

    //TODO: update workouts if name changed
    @PostMapping("/{name}")
    public @ResponseBody User updateUser(@PathVariable String name, @RequestBody User user) {
        LOGGER.info(String.format("Updating user - name:%s", name));

        if(!name.equals(user.getName())){
            throw new GainsLogException("Permission Denied - users do no match", ExceptionCode.AUTHENTICATION, null);
        }

        try {
            if (userService.updateUser(user) != 1) {
                throw new GainsLogException("Error creating user", ExceptionCode.GENERIC, null);
            }
        }catch (GainsLogException e){
            throw e;
        }catch (Exception e){
            handleException(new GainsLogException("Error creating user", ExceptionCode.GENERIC, e));
        }

        return user;
    }
}
