package sheldon.hampton.enums;

/**
 * @author Sheldon
 * @date 0029, 29, January, 2018
 */
public enum RoleTypeEnum {
    Overlord, Warden, Lackey, Pleb, Vagabond
}
