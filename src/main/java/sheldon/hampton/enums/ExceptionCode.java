package sheldon.hampton.enums;

/**
 * @author Sheldon
 * @date 0002, 2, February, 2018
 */
public enum ExceptionCode {
    GENERIC, AUTHENTICATION, AUTHORIZATION, BAD_REQUEST
}
