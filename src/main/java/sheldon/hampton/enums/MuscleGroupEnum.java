package sheldon.hampton.enums;

/**
 * @author Sheldon
 * @date 0007, 7, December, 2016
 */
public enum MuscleGroupEnum {
    Arms, Back, Chest, Core, Heart, Legs, Shoulders
}
