package sheldon.hampton.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sheldon.hampton.entity.User;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogException;
import sheldon.hampton.utility.JsonUtil;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static sheldon.hampton.exception.GainsLogExceptionHandler.handleException;

/**
 * @author Sheldon
 * @date 0014, 14, January, 2018
 */
@Repository
@Transactional
public class UserDao extends BaseDao{
    @Value("${insert.user}")
    private String insertUserQuery;

    @Value("${select.user}")
    private String selectUserQuery;

    @Value("${update.user}")
    private String updateUserQuery;

    @Value("${delete.user}")
    private String deleteUserQuery;

    @Value("${select.all.users}")
    private String selectAllUsersQuery;

    public List<User> selectAllUsers(){
        List<User> users = new ArrayList<>(0);

        try {
            users = jdbcTemplate.query(selectAllUsersQuery, new UserDao.UserRowMapper());
        }catch (Exception e){
            handleException(new GainsLogException("Error selecting users", ExceptionCode.GENERIC, e));
        }

        return users;
    }

    public User selectUser(String name){
        User user = null;

        try {
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("name", name);
            user = namedParameterJdbcTemplate.queryForObject(selectUserQuery, paramMap, new UserDao.UserRowMapper());
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting name: %s", name), ExceptionCode.GENERIC, e));
        }

        return user;
    }

    public int createUser(String user){
        int result = -1;

        try {
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("user", user);
            result = namedParameterJdbcTemplate.update(insertUserQuery, paramMap);
        }catch (Exception e){
            handleException(new GainsLogException("Error creating user", ExceptionCode.GENERIC, e));
        }

        return result;
    }

    public int deleteUser(String name){
        int result = -1;

        try {
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("name", name);
            result = namedParameterJdbcTemplate.update(deleteUserQuery, paramMap);
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error deleting name: %s", name), ExceptionCode.GENERIC, e));
        }

        return result;
    }

    public int updateUser(String user, String name){
        int result = -1;

        try {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("user", user);
            paramMap.put("name", name);
            result = namedParameterJdbcTemplate.update(updateUserQuery, paramMap);
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error updating name: %s", name), ExceptionCode.GENERIC, e));
        }

        return result;
    }

    class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = null;
            try {
                user = (User) JsonUtil.mapStrToObj(rs.getString("user_json"), User.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return user;
        }
    }
}
