package sheldon.hampton.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sheldon.hampton.entity.Workout;
import sheldon.hampton.enums.ExceptionCode;
import sheldon.hampton.exception.GainsLogException;
import sheldon.hampton.utility.JsonUtil;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static sheldon.hampton.exception.GainsLogExceptionHandler.handleException;

/**
 * @author Sheldon
 * @date 0012, 12, March, 2017
 */
@Repository
@Transactional
public class WorkoutDao extends BaseDao{

    @Value("${insert.workout}")
    private String insertWorkoutQuery;

    @Value("${select.workout}")
    private String selectWorkoutQuery;

    @Value("${update.workout}")
    private String updateWorkoutQuery;

    @Value("${delete.workout}")
    private String deleteWorkoutQuery;

    @Value("${select.all.workouts}")
    private String selectAllWorkoutsQuery;

    public List<Workout> selectAllWorkouts(String userName){
        List<Workout> workouts = new ArrayList<>(0);

        try {
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("userName", userName);
            workouts = namedParameterJdbcTemplate.query(selectAllWorkoutsQuery, paramMap, new WorkoutRowMapper());
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting workouts for user: %s", userName), ExceptionCode.GENERIC, e));
        }

        return workouts;
    }

    public Workout selectWorkout(String userName, String name){
        Workout workout = null;

        try {
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("name", name);
            paramMap.put("userName", userName);
            workout = namedParameterJdbcTemplate.queryForObject(selectWorkoutQuery, paramMap, new WorkoutRowMapper());
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error selecting workout: %s for user: %s", name, userName), ExceptionCode.GENERIC, e));
        }

        return workout;
    }

    public int createWorkout(String workout){
        int result = -1;
        try {
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("workout", workout);
            result = namedParameterJdbcTemplate.update(insertWorkoutQuery, paramMap);
        }catch (Exception e){
            handleException(new GainsLogException("Error creating workout", ExceptionCode.GENERIC, e));
        }
        return result;
    }

    public int deleteWorkout(String userName, String name){
        int result = -1;
        try {
            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("name", name);
            paramMap.put("userName", userName);
            result = namedParameterJdbcTemplate.update(deleteWorkoutQuery, paramMap);
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error deleting workout: %s for user: %s", name, userName), ExceptionCode.GENERIC, e));
        }
        return result;
    }

    public int updateWorkout(String workout, String userName, String name){
        int result = -1;
        try {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("workout", workout);
            paramMap.put("name", name);
            paramMap.put("userName", userName);
            result = namedParameterJdbcTemplate.update(updateWorkoutQuery, paramMap);
        }catch (Exception e){
            handleException(new GainsLogException(String.format("Error updating workout: %s for user: %s", name, userName), ExceptionCode.GENERIC, e));
        }
        return result;
    }

    class WorkoutRowMapper implements RowMapper<Workout> {

        @Override
        public Workout mapRow(ResultSet rs, int rowNum) throws SQLException {
            Workout workout = null;
            try {
                workout = (Workout) JsonUtil.mapStrToObj(rs.getString("workout"), Workout.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return workout;
        }
    }
}
