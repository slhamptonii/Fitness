package sheldon.hampton.utility;


import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author Sheldon
 * @date 0012, 12, March, 2017
 */
@Component
public class JsonUtil {

    private static ObjectMapper objectMapper;

    public JsonUtil(){
        objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
    }

    public static String mapObjToStr(Object obj) throws IOException {
        if(null == objectMapper){
            objectMapper = new ObjectMapper();
            objectMapper.enable(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        }
        return objectMapper.writeValueAsString(obj);
    }

    public static Object mapStrToObj(String str, Class<?> clazz) throws IOException {
        if(null == objectMapper){
            objectMapper = new ObjectMapper();
            objectMapper.enable(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        }
        return objectMapper.readValue(str.getBytes(), clazz);
    }
}
